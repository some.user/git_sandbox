package main

import (
   "encoding/json"
   "flag"
   "net/http"
   "strconv"
   "strings"
)

// Описываем структуру данных. Указываем представление json
type Data struct {
   GoodsIds []uint64 `json:"goodsIds"`
}

type adGoods struct {
   Data `json:"data"`
}
// Описание основного приложение, поднятие web сервера и его конфиг
func main() {
   http.HandleFunc("/search/api/ad-goods", StubSearch)
   http.ListenAndServe(":8090", nil)
}

func StubSearch(w http.ResponseWriter, r *http.Request) {

   // объявляем переменную которая будет хранить переданные при запуске id товаров для ответа
   var needGoodsIds string 
   // Получаем переданные значения при запуске приложения -ids
   flag.StringVar(&needGoodsIds, "ids", "1,2", "Вказати айді, що треба повернути")
   flag.Parse()

   //Это нужно чтобы преобразовать переданную строку в массив с числовыми значениями
   strs := strings.Split(needGoodsIds, ",")
   var ids []uint64
   for _, s := range strs {
      num, err := strconv.Atoi(s)
      if err == nil {
         ids = append(ids, uint64(num))
      }
   }
   
   // формируем ответ для заглушки
   responce := adGoods{Data{GoodsIds: ids}}

   // формируем json ответ
   js, err := json.Marshal(responce)
   if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
   }

   w.Header().Set("Content-Type", "application/json")
   // отдаем сформированный json ответ
   w.Write(js)
}
